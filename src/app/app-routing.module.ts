import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ShopComponent } from './components/shop/shop.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { CartComponent } from './components/cart/cart.component';
import { PromptPageComponent } from './prompt-page/prompt-page.component';
import { CreditcardPageComponent } from './creditcard-page/creditcard-page.component';
import { QrPageComponent } from './qr-page/qr-page.component';
import { ThanksComponent } from './components/thanks/thanks.component';



const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'details', component: ProductDetailsComponent },
  { path: 'cart', component: CartComponent },
  { path: 'prompt', component: PromptPageComponent },
  { path: 'creditcard', component: CreditcardPageComponent },
  { path: 'qrcode', component: QrPageComponent },
  { path: 'thanks', component: ThanksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
