import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { HomeComponent } from './components/home/home.component';
import { ShopComponent } from './components/shop/shop.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { SliderComponent } from './components/slider/slider.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';
import { ProductService } from '././services/product.service';
import { HttpClientModule } from '@angular/common/http';
import { PromptPageComponent } from './prompt-page/prompt-page.component';
import { QrPageComponent } from './qr-page/qr-page.component';
import { CreditcardPageComponent } from './creditcard-page/creditcard-page.component';
import { ThanksComponent } from './components/thanks/thanks.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ShopComponent,
    AboutComponent,
    ContactComponent,
    SliderComponent,
    ProductDetailsComponent,
    ProductsComponent,
    CartComponent,
    PromptPageComponent,
    QrPageComponent,
    CreditcardPageComponent,
    ThanksComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UiModule,
    NgbModule,
    HttpClientModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [ProductService, ProductsComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
