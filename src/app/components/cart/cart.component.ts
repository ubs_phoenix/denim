import { Component, OnInit } from '@angular/core';

import { Item } from '../../entities/item.entity';
import { ProductService } from '../../services/product.service';
import { ActivatedRoute } from '@angular/router';
import { ProductsComponent } from '../products/products.component';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';


@Component({
	selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

	public items: Item[] = [];
	public total: number = 0;
	response: any;
	data: any;

	constructor(
		private activatedRoute: ActivatedRoute,
		private productService: ProductService
	) { }

	ngOnInit() {

	//	setTimeout(() => {

			this.activatedRoute.params.subscribe(params => {
				let id: number = params['id'];
				console.log(id);
			
					  if (id) {
						let item: Item = {
								product: this.productService.find(id),
								quantity: 1
					};
					// console.log(item);
					// if (localStorage.getItem('cart') == null) {
						let cart: any = [];
						  cart.push(JSON.stringify(item));
						localStorage.setItem('cart', JSON.stringify(cart));

						 var data = {
							"productId" : +id,
							"quantity" : 1
						}
						console.log(data);
						this.productService.getTransaction(data).subscribe(
							(res) => {
							 this.data = res,
							 console.log(this.data)
						   });
					//}
			// 		else {
			// 			let cart: any = JSON.parse(localStorage.getItem('cart'));
			//   let index: number = -1;
			//   console.log(cart)
			// 			for (let i = 0; i < cart.length; i++) {
			// 	let item: Item = JSON.parse(cart[i]);
			// 	console.log(item.product);
			// 				if (item.product.id == id) {
			// 					index = i;
			// 					break;
			// 				}
			// 			}
			// 			if (index == -1) {
			// 				cart.push(JSON.stringify(item));
			// 				localStorage.setItem('cart', JSON.stringify(cart));
			// 			} else {
			// 				let item: Item = JSON.parse(cart[index]);
			// 				item.quantity += 1;
			// 				cart[index] = JSON.stringify(item);
			// 				localStorage.setItem("cart", JSON.stringify(cart));
			// 			}
			// 		}
	   
						  this.loadCart();
					  } else {
						  this.loadCart();
					  }
				  });
			
		//}, 3000);
			
		

	}
	gettrans(data = this.data){
		this.productService.getTransaction(data).subscribe(
			  (res) => {
			   this.response = res,
			   console.log(this.response)
			 });

	}
	

	loadCart(): void {
		setTimeout(() => {
			this.total = 0;
		this.items = [];
		let cart = JSON.parse(localStorage.getItem('cart'));
		for (let i = 0; i < cart.length; i++) {
			let item = JSON.parse(cart[i]);
			this.items.push({
				product: item.product,
				quantity: item.quantity
			});
			this.total += item.product.price * item.quantity;
		}
		}, 2000);
		
	}

	remove(id: string): void {
		let cart: any = JSON.parse(localStorage.getItem('cart'));
		let index: number = -1;
		for (let i = 0; i < cart.length; i++) {
			let item: Item = JSON.parse(cart[i]);
			if (item.product.id == id) {
				cart.splice(i, 1);
				break;
			}
		}
		localStorage.setItem("cart", JSON.stringify(cart));
		this.loadCart();
	}

	checkout(){
		alert("Your Total Price "+this.total);
	}


}