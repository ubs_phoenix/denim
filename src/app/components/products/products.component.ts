import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Product } from '../../entities/product.entity';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  response: any = [];
   products: Product[];
  constructor(private productService: ProductService) { 

    // this.productService.getProducts()
    // .subscribe(
    //   (res) => {
    //   this.response = res
    // });
 }

 


  ngOnInit() {
    
    setTimeout(() => {

      this.products = this.productService.findAll();
      
    }, 2000);
    
  }


  

}
