import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-prompt-page',
  templateUrl: './prompt-page.component.html',
  styleUrls: ['./prompt-page.component.css']
})
export class PromptPageComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) { }

  tID: number;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      let id: number = params['id'];
      console.log(id);
    
         this.tID=id;
       console.log(this.tID);
        });
  }

}
