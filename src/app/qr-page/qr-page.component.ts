import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { timer, of } from 'rxjs';


@Component({
  selector: 'app-qr-page',
  templateUrl: './qr-page.component.html',
  styleUrls: ['./qr-page.component.css']
})
export class QrPageComponent implements OnInit {

  constructor(private http: ProductService, private domSanitizer: DomSanitizer, private activatedRoute: ActivatedRoute, private router: Router) { }

  imageData: any;
  subscribe: any;
  qid: number;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      let id: number = params['id'];
      console.log("in QR ID");
      console.log(id);
      this.qid=id;
    
      this.http.getQR(id).subscribe(
        (res) => {
          this.createImageFromBlob(res);
          let urlCreator = window.URL;
          this.imageData = this.domSanitizer.bypassSecurityTrustUrl(
            urlCreator.createObjectURL(res));
      
          //Cast to a File() type
          console.log(<File>res);
        }
      )
       
        });

        const source = timer(0, 1000);
        this.subscribe = source.subscribe( t=>
          this.setdata(this.qid)
        );
        console.log('Data Response');

  }

  setdata(id){
    this.http.getQRStat(id).subscribe(data =>{
      console.log(data);
      if(data == true){
        console.log('Thanks for Shopping')
        this.subscribe.unsubscribe();
        this.router.navigate(['/thanks']);
      }
    })   
  }

  imageToShow: any;

createImageFromBlob(image: Blob) {
   let reader = new FileReader();
   reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
   }, false);

   if (image) {
      reader.readAsDataURL(image);
   }
}

}
