import { Injectable } from '@angular/core';

import { Product } from '../entities/product.entity';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable()
export class ProductService {
    response: any = [];
    private products: Product[];

    //private products: any;

    private baseUrl ="http://18.188.157.205:8080";

    //response: any;

    constructor(private http: HttpClient) {
        
        this.getProducts()
        .subscribe(
          (res) => {
          this.response = res,
          console.log("in products")
         
         
            this.products = [
              { id: this.response[0]["id"], name: this.response[0]["name"], price: this.response[0]["price"], photo: 'jean6.jpg' },
              { id: this.response[1]["id"], name: this.response[1]["name"], price: this.response[1]["price"], photo: 'jean6.jpg' },
              { id: this.response[2]["id"], name: this.response[2]["name"], price: this.response[2]["price"], photo: 'jean6.jpg' },
              { id: this.response[3]["id"], name: this.response[3]["name"], price: this.response[3]["price"], photo: 'jean6.jpg' },
              { id: this.response[4]["id"], name: this.response[4]["name"], price: this.response[4]["price"], photo: 'jean6.jpg' },
              { id: this.response[5]["id"], name: this.response[5]["name"], price: this.response[5]["price"], photo: 'jean6.jpg' },
           
          ];
    
        
        });
    
    }
    private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

    getProducts(){
        return this.http.get(this.baseUrl+"/seqr/product/list");
        // .pipe(map(
        //     (res) => {
        //         this.response = res;
        //         console.log("In service");
        //         console.log(this.response)
        //     }
        // ))
    }
    getQRStat(id){
        return this.http.get(this.baseUrl+'/seqr/transaction/payment/check?transactionId='+id).
        pipe(map(res=> res));
    }

    getQR(id){
        return this.http.get(this.baseUrl+"/seqr/transaction/receipt/qr?id="+id, { responseType: 'blob' });
    }
    getTransaction(data){
        return this.http.post(this.baseUrl+"/seqr/transaction/process/single",data,this.options);
    }

     findAll(): Product[] {
        return this.products;
     }

     find(id: number): Product {
         setTimeout(() => {

            console.log("in find");
         console.log(this.products[this.getSelectedIndex(id)]);
             
         }, 2000);
         console.log(this.products[this.getSelectedIndex(id)]);
         return this.products[this.getSelectedIndex(id)];
     }

    

     private getSelectedIndex(id: number) {

       
        setTimeout(() => {
            console.log("in select index");
            console.log(this.products.length);
            
            
        }, 2000);
       
         for (let i = 0; i < this.products.length; i++) {
            console.log(this.products.length);
            if (this.products[i].id == id) {
                 return i;
             }
         }
        return -1;
     }

}